#Un perrito y un pajarito

##Inciso A

Podemos decir que  el poste A está en el punto (0,-1/2) y que el poste B está en el punto (0,1/2). 
Para todo punto $(x,y)$ donde pueda pararse el perro debe cumplirse que 
$$(x - 1/2)^2 + y^2 < 1 $$
debido a que estar atado al poste B condiciona al perro a poder moverse solo en el interior de un círculo de radio 1 centrado  en (0,1/2). De manera similar, estar atado al poste A implica que debe cumplirse 

$$(x + 1/2)^2 + y^2 < 1 $$

Podemos calcular analíticamente cuál es el área de la intersección de estos dos círculos de la siguiente manera:

$$\int _{-\sqrt{\frac{3}{4}}}^{\sqrt{\frac{3}{4}}}\int _{\frac{1}{2}-\sqrt{1-y^2}}^{\sqrt{1-y^2}-\frac{1}{2}} dxdy = \frac{1}{6} \left(4 \pi -3 \sqrt{3}\right)=1.228374... $$

Tambien podemos integrar númericamente usando diversos métodos, en el programa 2ab.py se implementa una versión simple del método de montecarlo y el método de Newton-Cotes de orden 0. Existen muchos otros métodos de integración numérica que podrían implementarse, y cada uno de ellos se podría generalizar mucho (por ejemplo, que se pueda ingresar el orden como argumento al método de Newton-Cotes ), pero sería reinventar la rueda ya que esos métodos ya fueron implementados. En la librería SciPy se pueden encontrar varios métodos de integración

##Inciso B

De manera similar al inciso anterior, las dos condiciones que deben cumpirse son:

$$\left(x-\frac{1}{2}\right)^2+y^2+z^2<1$$
$$\left(x+\frac{1}{2}\right)^2+y^2+z^2<1$$

y el espacio en que puede moverse es la intersección entre dos esferas. Podemos calcular el volumen analíticamente  como :

$$\int _{-\sqrt{\frac{3}{4}}}^{\sqrt{\frac{3}{4}}}\int _{-\sqrt{\frac{3}{4}-z^2}}^{\sqrt{\frac{3}{4}-z^2}}\int _{\frac{1}{2}-\sqrt{1-\left(y^2+z^2\right)}}^{\sqrt{1-\left(y^2+z^2\right)}-\frac{1}{2}}dxdydz=\frac{5 \pi }{12}=1.309...$$

En el programa 2ab.py se implementa el método de integración por Montecarlo.


