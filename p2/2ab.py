from __future__ import division
from random import uniform
from math import sqrt

def is_inside2D(x,y):
	"""
	Input: coordenadas x,y
	Output: True si el punto (x,y) esta dentro del area en que puede moverse el perro y False si no lo esta
	"""
	return (x-.5)**2 + y**2 < 1 and (x+.5)**2 + y**2 < 1

def is_inside3D(x,y,z):
	"""
	Input: coordenadas x,y,z
	Output: True si el punto (x,y,z) esta dentro del area en que puede moverse el pajaro y False si no lo esta
	"""
	return (x-.5)**2 + y**2 + z**2 < 1 and (x+.5)**2 + y**2 + z**2 < 1

def montecarlo(test, square,total  = 100000):
	"""
	Input:	test, una funcion de 2 o 3 variables que indique si un punto dado esta dentro del conjunto o no
			square, un cuadrado o cubo que contenga el conjunto a medir. El formato es (x0,y0,x1,y1) o (x0,y0,z0,x1,y1,z1)
			siendo x0,y0,z0 las coordenadas del vertice con menor valor de coordenadas y
			siendo x1,y1,z1 las coordenadas del vertice con mayor valor de coordenadas)
	Output: la medida del conjunto
	"""
	if len(square)== 4: x0,y0,x1,y1			= square
	if len(square)== 6: x0,y0,z0,x1,y1,z1	= square
	inside = 0
	for _ in range(total):
		if len(square)== 4: random_point = (uniform(x0,x1),uniform(y0,y1))
		if len(square)== 6: random_point = (uniform(x0,x1),uniform(y0,y1),uniform(z0,z1))
		if test(*random_point):
			inside += 1
	if len(square)== 4: total_measure = (x1-x0)*(y1-y0)
	if len(square)== 6: total_measure = (x1-x0)*(y1-y0)*(z1-z0)
	return total_measure * (inside/total)

# integracion numerica

def y(x):
	return sqrt(1-(x+0.5)**2)

def newton_cotes(f,a,b,n=1000):
	"""
	metodo de Newton-Cotes de orden 0 para integrar una funcion f, de una variable, en el intervalo (a,b)
	Input:	funcion f, de una variable.
			intervalo de integracion (a,b)
	Output:	valor de la integral
	"""
	dx= (b-a)/n
	s = 0
	for i in range(n):
		s += f(a+dx*i)
	return s * dx

print "para el perrito:"
print "\tmontecarlo:\t",montecarlo(is_inside2D, (-.5,-sqrt(3)/2,.5,sqrt(3)/2))
print "\tnewton-cotes:\t",4 * newton_cotes(y,0,0.5)
print "para el pajaro:"
print "\tmontecarlo:\t",montecarlo(is_inside3D, (-.5,-sqrt(3)/2,-sqrt(3)/2,.5,sqrt(3)/2,sqrt(3)/2) )
