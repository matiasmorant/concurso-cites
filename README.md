#Cites code contest

1. El primo de Fibonacci (Teoría de números)
2. Un perrito y un pajarito (Cálculo numérico)
3. La inclinación del cilindro (3D scanning/printing)
4. Recordando a Dennis (Criptografía y seguridad)
5. El problema del viajante argentino (Optimización e inteligencia artificial)
