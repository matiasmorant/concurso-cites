#Recordando a Dennis

>**Nota:** Para resolver este problema se optó usar el programa [oclHashcat](http://hashcat.net/oclhashcat/), que es libre, de código abierto, multiplataforma y está muy bien optimizado para correr en GPU. Para que la respuesta al problema no termine siendo simplemente una descripción de cómo usar un programa, se decidió presentar la respuesta a modo informativo, de manera que sea fácil de entender y entretenida  para una persona sin ningún conocimiento de criptografía. 

-----

Cifrar un mensaje significa cambiar la forma del mensaje, de forma que solo lo entienda la persona que lo tiene que recibir. Cualquier sitio web, red social, o programa que necesite que pongas una contraseña, tiene que tener guardada esa contraseña de alguna forma en su base de datos, para poder saber si la contraseña que estas poniendo es la correcta.


Para cifrar una contraseña lo que se hace es pasarla por un algoritmo que se llama [función hash](https://es.wikipedia.org/wiki/Funci%C3%B3n_hash), que toma como dato de entrada la contraseña y genera un hash.

Un hash es un montón de letras, número y símbolos inentendible, pero que llevan la información de la contraseña.

La idea de la función hash es que tiene que ser prácticamente imposible encontrar la función inversa, es decir que no podes, según el hash, encontrar cual la contraseña. Así,  si alguien se las arregla para encontrar el hash de una contraseña, no le es útil porque no le sirve para  generar la contraseña.

Pero este método de cifrado tiene un punto débil, si tenés el hash de una contraseña, y conoces cual es la función hash que lo genero, se pueden probar muchas contraseñas hasta que eventualmente se encuentre la contraseña que genera el hash igual al que se tiene.

A medida que las computadoras se fueron haciendo más baratas y potentes, se fueron necesitando mejorar los sistemas de cifrado, haciendo más complejas las funciones hash.

###Resolución del problema
Tenemos las líneas del archivo `/etc/shadow`, lo que contiene este archivo son tres hash de contraseñas de Dennis. .  Los tres hash corresponden a tres tipos de función hash distintos. El primero es de cifrado [DES](https://es.wikipedia.org/wiki/Data_Encryption_Standard), el segundo [MD5](https://es.wikipedia.org/wiki/MD5) y el tercero es [SHA512](https://es.wikipedia.org/wiki/Secure_Hash_Algorithm).

Para hacer el ataque de fuerza bruta usamos el programa oclHascat, que es libre y open source, y está hecho para correr en GPU. También se puede usar Hashcat, que corre en CPU.

Al programa se le indica qué tipo de función hash corresponde, y que tiene que probar contraseñas conformadas por letras minúsculas como máximo de 6 caracteres.
Ejecutamos el programa y encontró la clave en 8 segundos:

![fido](http://i.imgur.com/CpXHM7e.png)

El perro de Dennis es **fido**. Corremos el programa con el segundo hash, indicando que es de tipo MD5, y después de 5 segundos tenemos la clave:

![white](http://i.imgur.com/Vs6FV4n.png)

El color favorito de Dennis es **white**. Finalmente corremos el programa con el último hash, indicando que es el SHA512:

![knicks](http://i.imgur.com/LQ3DhBw.png)

Y después de tres horas y media tenemos última clave, Dennis era hincha de los **knicks**. Es necesario mucho más tiempo que los casos anteriores, pero sigue siendo tan fácil como dejar corriendo un programa.

###Análisis de la resolución
El problema se resolvió rápidamente gracias a la paralelización en GPU (placa AMD Radeon R9 270X) y a los constraints que nos dieron sobre las contraseñas.
La cantidad posible de combinaciones de contraseñas minúsculas (26 letras sin contar la ñ), y de longitud seis, es:

$$
 26*26*26*26*26*26\approx309*10^6
$$
A esto le sumamos los casos de longitud 5, 4, 3, etc., y llegamos a 320 millones de combinaciones posibles. Este número es bastante chico. Para tener una idea, va una tabla con la cantidad de posibles contraseñasen otros casos:

**Cantidad de posibles contraseñas (millones)**

| Tipo \ Longitud        |6     |7        |    8     |
| :--------------------- |:---- |:------- |:-------- |
| Solo minúsculas        |309   |8 032    |208 827   |
| Minúsculas y mayusculas|19 771|1 028 072|53 459 729|

Entonces si hubiésemos tenido que probar contraseñas de 8 caracteres y minúsculas y mayúsculas mezcladas, en el caso del hash de SHA512, y en la misma GPU,  para probar todas las combinaciones posibles necesitaríamos 216 años.

El método que usamos ahora se llama fuerza bruta y es el más simple, pero también existen métodos más efectivos y complejos, como usar [ataque de diccionarios](https://es.wikipedia.org/wiki/Ataque_de_diccionario), usar [tablas rainbow](https://es.wikipedia.org/wiki/Tabla_arco%C3%ADris), e inclusive [redes neuronales](https://en.wikipedia.org/wiki/Neural_cryptography).

Dados los constraints que teníamos, podríamos haber resuelto el último problema con un ataque de diccionarios.
