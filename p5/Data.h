#ifndef DATA_H
#define DATA_H

/* Clase Data:
 * clase para manejo de datos inputs y outputs del problema.
 */
class Data{
	public:
		/* Funciones */
		Data(); // Constructor
		void loadData(); // Carga de datos
		void setConstraints(); // Seteo de restricciones
		void makeCostTable(); // Creación de tabla de costos
		
		/* Variables */
		ifstream file; // Objeto para operar archivo de input
		int nCities; // Cantidad de ciudades a visitar
		float* x; // Arreglo con coordenadas x de las ciudades
		float* y; // Arreglo con coordenadas y de las ciudades
		int* constraints; // Arreglo con restricciones en ciudades
		string* cities; // Nombres de ciudades
		double* prices; // Tabla de costos de caminos entre ciudades en representación vectorial
};

#endif
