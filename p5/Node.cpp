#ifndef NODE_CPP
#define NODE_CPP

/* Constructor Node:
 * no realiza acción.
 * input: vacío.
 */
Node::Node(){}

/* Constructor Node:
 * constructor alternativo, para seteo de nombre de nodo.
 * input: n (nombre de nodo).
 */
Node::Node(int n){
	
	name = n;
}

/* Función setNext:
 * seteo de nodo siguiente. Evalua si ya existía un nodo apuntado, y en este caso se asegura de que aquel ya no apunte al propio nodo mediante prev.
 * input: N (puntero a nodo siguiente), oldNode (identificador de nodo como previamente inicializado - mayor a 0 indica ya incializado).
 * output: -
 */
void Node::setNext(Node* N, int oldNode){
	
	if(this->next != NULL && oldNode>0) if(this->next->prev->name == this->name) this->next->prev=NULL;
	this->next = N;
	if(N != NULL) N->prev=this;
}

/* Función setPrev:
 * seteo de nodo previo. Evalua si ya existía un nodo apuntado, y en este caso se asegura de que aquel ya no apunte al propio nodo mediante next.
 * input: N (puntero a nodo previo).
 * output: -
 */
void Node::setPrev(Node* N){
	
	if(this->prev != NULL) this->prev->next=NULL;
	this->prev = N;
	if(N != NULL) N->next=this;
}

/* Función jump:
 * devuelve un puntero al nodo N posiciones después.
 * input: N (cantidad de eslabones a saltar, admite negativos)
 * output: aux, puntero a nodo deseado.
 */
Node* Node::jump(int N){
	
	Node* aux = this;
	for (int i=0; i*i<N*N; i++) aux = N>0 ? aux->next : aux->prev ;
  		
	return aux;
}

/* Función swap:
 * intercambia punteros prev, next.
 * input: -
 * output: -
 */
void Node::swap(){
	Node* aux = this->next;
	this->next = this->prev;
	this->prev = aux;
}

/* Función reverseList:
 * interambia sentido de porción de lista, empezando del segundo nodo proporcionado. No actúa en el nodo final.
 * input: i(nodo incial), j(nodo final).
 * output: -
 */
void reverseList(Node* i, Node* j){
	while(i->name != j->name){
		j->swap();
		j = j->next;
	}
}

#endif
