#ifndef DATA_CPP
#define DATA_CPP

/* Constructor Data:
 * no realiza acción.
 * input: -
 */
Data::Data(){}

/* Función loadData:
 * lee nombres de ciudades y coordenadas en archivo "coordenadas.txt".
 * input: - 
 * output: -
 */
void Data::loadData(){
  
	// Lectura de datos
	string stringAux;
	nCities = 0; // Cantidad de ciudades
	file.open( "coordenadas.txt" );	
	if (file.is_open()) {
		// Lectura de cantidad de ciudades
		while ( getline(file, stringAux)){
			nCities ++;
		}
		// Retorno a comienzo de archivo
		file.clear();
		file.seekg(0, ios::beg); 
		
		// Allocación de arreglos x, y, cities
		x = new float[nCities]; // Arreglo con coordenadas x de las ciudades
		y = new float[nCities]; // Arreglo con coordenadas 7 de las ciudades
		cities = new string[nCities]; // Nombres de ciudades
		// Carga de datos
		for (int i=0; i<nCities; i++){
			file >> x[i] >> y[i];
			getline(file, stringAux);
			cities[i] = stringAux;			
		}
		file.close();
	}
	else{
		cout<<"   Error al intentar abrir 'coordenadas.txt' "<<endl<<endl;
		exit(0);
	}
}

/* Función Data:
 * setea restricciones de camino. Debe ser modificada por el usuario.
 * input: -
 * output: -
 */
void Data::setConstraints(){
  
	// Allocación e inicialización de constraints
	constraints = new int[nCities]; //  Arreglo de restricciones
	for (int i=0; i<nCities; i++){
		constraints[i] = 0;
	}
	  
	// Seteo de restricciones para cada ciudad
	constraints[1] = 7;
	constraints[7] = 1;
	constraints[3] = 13;
	constraints[13] = 3;
	constraints[6] = 15;
	constraints[15] = 6;
	constraints[9] = 16; 
	constraints[16] = 9;
	constraints[11] = 18;
	constraints[18] = 11;
  
}

/* Función makeCostTable:
 * setea tabla de costos de caminos entre ciudades en representación vectorial.
 * input: -
 * output: -
 */
void Data::makeCostTable(){
	
	// Carga de tabla de costos de camino entre ciudades
	prices = new double[nCities*nCities]; //Tabla de costos en representación vectorial.
	for (int i=0; i<nCities; i++){
		for (int j=0; j<nCities; j++){
			if( i != j ){
				prices[i*nCities+j] = sqrt(pow(x[i]-x[j],2)+pow(y[i]-y[j],2)); // Distancia entre ciudades
				prices[i+nCities*j] = prices[i*nCities+j]; // Distancia entre ciudades, elemento conjugado
			}
			else{
				prices[i*nCities+j] = 0; // Distancia a la misma ciudad
			}			
		}
		// Carga de restricciones como caminos de elevado costo
		if(constraints[i]>0){
			prices[constraints[i]*nCities+i]=99999999999;
			prices[i*nCities+constraints[i]]=99999999999;
		}
	 }  
}


#endif
