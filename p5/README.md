# El problema del viajante argentino

Este problema es clásico. La búsqueda de solución por fuerza bruta es muy ineficiente ya que requiere de muchos recursos.
El problema aquí lo podemos resolver mediante un código Montecarlo. Dada una condición inicial el código realiza sorteos sucesivos de permutaciones de camino, buscando mejorar el costo de recorrido. Es probable caer en un mínimo local de solución y por lo tanto es fundamental evaluar diferentes condiciones iniciales.
El programa está escrito en lenguaje c++ y utiliza la biblioteca estándard.

El algoritmo implementado en main.cpp ejecuta los siguientes pasos:

1. Carga de datos:
	Creación de puntero a objeto de clase Data para manejo de datos: lectura de cantidad de ciudades en "coordenadas.txt", coordenadas y nombres. Allocación de arreglos necesarios para operar. 
2. Seteo de restricciones de camino.
3. Cálculo de tabla de costos de camino entre ciudades. Las restricciones se implementan como costos altamente elevados.
4. Creación de arreglo de objetos de clase TSP para búsquedas de camino:
	Cada objeto va a buscar el camino a partir de una condición inicial diferente.
	La idea es implementar paralelización en esta etapa del algoritmo, pero por cuestiones de tiempo no pudo realizarse.
5. Loop de implementaciones del algoritmo de búsqueda:
	a. Seteo de condición inicial.
	b. Búsqueda mediante algoritmo Montecarlo.
	c. Almacenamiento de la mejor solución hallada.
6. Comparación entre las mejores soluciones halladas y exportación del mejor camino en "path.txt".

Se desarrolló la clase Node para implementación de los diferentes nodos de una lista ordenada, en la que cada elemento cuenta con punteros a los dos elementos contiguos. Esta clase facilita la manipulación de los recorridos hallaodos.

El algoritmo de búsqueda Montecarlo está implementado en la función mc de la clase TSP. Básicamente:

1. 	Sortea una permutación de camino
2.	Evalúa la permutación: si el nuevo camino tiene menor costo de recorrido, la permutación es aceptada. Si el nuevo camino tiene mayor costo, se asigna una probabilidad a la permutación y se sortea la aceptación del cambio.
3.	Si la permutación es finalmente aceptada, se modifica el camino previo.
4.	Cada vez que el costo de recorrido es inferior al mejor costo almacenado para una dada condición inicial, se guardan los nuevos datos.

El programa puede compilarse en la forma usual mediante 
```
	g++ main.cpp -o main
```
El Makefile provisto compila junto con el profiler GNU gprof.

Ejecutar el programa de la forma usual meidante
```
	./main
```
El programa acepta hasta 3 argumentos (arg1, arg2, arg3).
Mediante arg1 es posible indicar cantidad de intentos con diferentes condiciones iniciales. Valor default arg1=10.
Mediante arg2 es posible indicar la constante kt para regular la aceptación de caminos (ver explicación en TSP.cpp - función evaluatePermutation). Valor default arg2=10.
Mediante arg3 es posible indicar cantidad de iteraciones máximas en cada intento con diferentes condiciones iniciales. Valor default arg3=10000.
