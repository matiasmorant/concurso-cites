#ifndef TSP_H
#define TSP_H

/* Clase TSP
 * clase para implementación del algoritmo Montecarlo para búsqueda del camino más corto entre ciudades.
 */
class TSP{
	public:
		/* Funciones */
		TSP(); // Constructor
		void initialCondition(int nCities, double* prices, int* constraints); // Sorteo de condición inicial
		double calculateCost(int nCities, Node* city, double* prices); // Cálculo de costos para un dado camino
		void mc(int nCities, double* prices); // Algoritmo Montecarlo de búsqueda
		int* rafflePermutation(int nCities); // Sorteo de permutaciones de camino
		double evaluatePermutation(int nCities, Node* city, int* permutation, double* prices, double cost); // Evaluación de permutaciones de camino
		void shiftBestPath(int nCities); // Ordenamiento de ciudades para situar el punto inicial de recorrido en elemento 0 
		
		/* Variables */
		float kt; // Factor en factor de Boltzmann
		Node* city; // Puntero a estructura ciudad
		double cost; // Costo de camino, medido como la distancia total del recorrido
		int maxIter; // Cantidad máxima de iteraciones
		int iter; // Iteración actual
		int* bestPath; // Arreglo de ciudades con mejor camino encontrado
		double bestCost; // Costo en mejor camino encontrado
};

#endif
