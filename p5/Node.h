#ifndef NODE_H
#define NODE_H

/* Clase Node:
 * clase para manejo de nodos en el camino. Cada instancia es elemento de una lista, con punteros hacia los elementos de lista contiguos. 
 */
class Node{
	public:
		/* Funciones */
		Node(); // Constructor
		Node(int n); // Constructor con seteo de nombre
		void setNext(Node* N, int oldNode); // Seteo de nodo siguiente
		void setPrev(Node* N); // Seteo de nodo previo
		Node* jump(int N); // Salto de N nodos
		void swap(); // Intercambio de objetos apuntados por prev, next
		
		/* Variables */
		int name; // Nombre del nodo
		Node* prev; // Puntero a nodo previo
		Node* next; // Puntero a nodo siguiente
};


#endif
