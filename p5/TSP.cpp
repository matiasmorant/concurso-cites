#ifndef TSP_CPP
#define TSP_CPP

/* Constructor TSP:
 * no realiza ninguna acción
 * input: -
 */
TSP::TSP(){}

/* Función initialCondition:
 * sortea la condición inicial de camino, respetando las restriccines.
 * input: nCities (cantidad de ciudades), prices (tabla de costos), constraints (restricciones).
 * output: -
 */
void TSP::initialCondition(int nCities, double* prices, int* constraints){
 
	// Arreglo para chequeo de ciudades sorteadas
	int* citiesRaffled = new int[nCities];
	for(int i=0; i<nCities; i++){
		citiesRaffled[i] = 0;
	}	
 
	// Sorteo del camino
	int aux, flag;
	city = new Node(0);
	for(int i=0; i<nCities-1; i++){		
		flag = 1;
		// Iteración buscando ciudades aún no sorteadas
		while(flag==1){
			flag=0;
			aux = (rand() % (nCities-1)+1);
			// Chequea si la ciudad ya fue seleccionada o si tiene restricción con la ciudad previa
			if((citiesRaffled[aux] == 1) || (i>0 && constraints[city->prev->name]==aux) ) {
				flag = 1;
			}
		}
		// Seteo de la ciudad
		citiesRaffled[aux] = 1;
		city->setNext(new Node(aux), 0);
		city = city->next;
	}
	
	// Seteo de conexiones en última ciudad
	city->setNext(city->jump(-nCities+1), 0);
	
	// Cálculo de costo de camino
	cost = calculateCost(nCities, city, prices);
	
	// Allocación e inicialización de bestPath
	bestPath = new int[nCities];
	for(int i=0; i<nCities; i++){
		bestPath[i] = city->name;
		city=city->next;
	}
}

/* Función calculateCost:
 * calcula el costo de camino como la suma de las distancias en el recorrido proporcionado.
 * input: nCities(cantidad de ciudades) , city(puntero a nodo de la lista), prices (tabla de costos de camino).
 * output: cost(costo de camino).
 */
double TSP::calculateCost(int nCities, Node* city, double* prices){
	
	double cost = 0.0;
	
	// Suma de caminos parciales. Considera el recorrido cerrado.
	for(int i=0; i<nCities; i++){
		cost += prices[city->name*nCities+city->next->name];
		city = city->next;
	}
	
	return cost;
}

/* Función mc:
 * algoritmo Montecarlo que realiza búsquedas de mejores caminos sorteando permutaciones.
 * input: nCities(cantida de ciudades), prices(tabla de costos de camino).
 * output: -
 */
void TSP::mc(int nCities, double* prices){
  
	double newCost = cost+1; // Costo del camino con correción de permutación.
	int* permutation = new int[2]; // Vector de permutaciones: (nodo inicial, nodo final).
	bestCost = cost; // Mejor costo hallado
	iter = 0; // Iteración actual
  
	// Iteraciones de búsqueda de mejor camino
	while( iter < maxIter ){
		permutation = rafflePermutation(nCities); // Sorteo de permutación
		cost = evaluatePermutation(nCities, city, permutation, prices, cost); // Evaluación de la permutación
		// Si el nuevo camino mejora el costo, guardar datos
		if(cost<bestCost) { 
			bestCost = cost;
			for(int i=0; i<nCities; i++){
				bestPath[i] = city->name;
				city = city->next;
			}
		}
		iter++;
		cout<<"\r Iteracion: "<<iter<<"  Ultimo costo: "<<cost<<" Mejor costo en intento: " <<bestCost;
	}
	shiftBestPath(nCities); // Ordenamiento de ciudades para situar el punto inicial de recorrido en elemento 0 
}

/* Función rafflePermutation:
 * sortea una permutación de camino.
 * input: nCities(cantidad de ciudades).
 * output: permutation (nodo i, nodo j).
 */
int* TSP::rafflePermutation(int nCities){
	
	int* permutation = new int[2]; // Arreglo de permutaciones
	
	// Sorteo
	permutation[0] = rand() % nCities;
	permutation[1] = rand() % nCities;
	while(permutation[1]==permutation[0]){ // Permutación no válida
		permutation[1] = rand() % nCities;
	}
	// Ordenamiento de las permutaciones
	if(permutation[0]>permutation[1]){ 
		int aux = permutation[0];
		permutation[0] = permutation[1];
		permutation[1] = aux;
	}

	return permutation;		
}

/* Función evaluatePermutation:
 * 
 * input: nCities(cantidad de ciudades), city (putero a nodo de lista), permutation (permutación), prices (tabla de costos de camino), cost (costo).
 * output: cost(costo de camino tras la evaluación).
 */
double TSP::evaluatePermutation(int nCities, Node* city, int* permutation, double* prices, double cost){
	
	// Búsueda de punteros a nodos de permutación y nodos siguientes
	Node *iCity; // Puntero a ciudad inicial en la permutación
	Node *jCity;  // Puntero a ciudad final en la permutación
	Node *iNextOld;  // Puntero a ciudad siguiente a la inicial en la permutación
	Node *jNextOld;  // Puntero a ciudad siguiente a la final en la permutación
	for(int k=0; k<nCities; k++){
		if(city->name==permutation[0]) iCity = city;
		else if(city->name==permutation[1]) jCity = city;
		city = city->next;
	}	
	iNextOld = (iCity->next);
	jNextOld = (jCity->next);
	
	// Chequeo de permutaciones que no generan cambio
	if(iNextOld == jCity) return cost; // La permutación no cambia el camino
	if(jCity == iCity->prev) return cost; // La permutación solo cambia el sentido del recorrido
	
	// Evaluación de costos de porciones de camino intercambiadas
	double price_I_J = prices[iCity->name*nCities+jCity->name]; // Costo de camino (i) -> (j)
	double price_I_INextOld = prices[iCity->name*nCities+iCity->next->name]; // Costo de camino (i->next) -> (j->next)
	double price_J_JNextOld = prices[jCity->name*nCities+jCity->next->name]; // Costo de camino (i) -> (j->next)
	double price_INextOld_JNextOld = prices[iCity->next->name*nCities+jCity->next->name]; // Costo de camino (i->next) -> (j)
	
	// Evaluación del costo de camino total si la permutación es aceptada
	double newCost = cost - price_I_INextOld - price_J_JNextOld + price_I_J + price_INextOld_JNextOld;
	
	// Probabilidad de aceptación de la permutación
	// Si el nuevo costo es inferior al previo la permutación se acepta (la probabilidad queda con valor superior a 1)
	// Si el nuevo costo es superior al previo la permutación toma una probabilidad inferior a 1
	// El valor kt regula la probabilidad de aceptación y es seteado en main.cpp o desde linea de comando como segundo argumento
	double permutationProbability = exp(-(newCost-cost)/kt);
	
	// Sorteo para evaluación de aceptación de permutación
	// Si el nuevo camino tiene menor costo, la permutación siempre es aceptada (rand()/RAND_MAX acotado por 1)
	if(((float)rand()/RAND_MAX) < permutationProbability){
		// Interambia sentido de porción de lista, empezando del segundo nodo proporcionado. No actúa en el nodo final
		reverseList(iCity, jCity);
		// Seteo de next en nodo inicial
		iCity->setNext(jCity, 1);
		// Seteo de next en nodo originariamente siguiente al inicial
		iNextOld->setNext(jNextOld, 1);
		// Seteo del nuevo costo
		cost = newCost;
	}
	
	return cost;
}

/* Función shiftBestPath:
 * Ordenamiento de ciudades para situar el punto inicial de recorrido en elemento 0 .
 * input: nCities (cantidad de ciudades).
 * output: -
 */
void TSP::shiftBestPath(int nCities){
	
	int* temp = new int[nCities];
	int shift;
	
	// Bísqueda de posición del nodo que es punto inicial
	for(int i=0; i<nCities; i++){
		temp[i] = bestPath[i];
		if(bestPath[i]==0) shift = i;
	}
	// Cambio de posiciones de la lista, sin alterar el orden
	for(int i=0; i<nCities; i++){
		bestPath[i] = temp[(i+shift)%nCities];
	}
}

#endif
