#include <stdlib.h> /* atoi, shuffle */
#include <math.h> /* pow, exp */
#include <time.h> /* seed */
#include <string> /* string */
#include <iostream> /* cin, cout */
#include <fstream> /* ifstream */
using namespace std;
#include "Node.h" /* clase Node */
#include "Node.cpp"
#include "Data.h" /* clase Data */
#include "Data.cpp"
#include "TSP.h" /* clase TSP */
#include "TSP.cpp"

#undef __FUNCT__
#define __FUNCT__ "main"

int main(int argc, char** argv){
	
	// Impresión
	cout<<"\n Resolviendo el problema del viajero comerciante con restricciones de camino"<<endl;
	
	// Creación de puntero a objeto de clase Data
	Data* data = new Data();

	// Lectura de datos
	data->loadData();
  
	// Restricciones
	data->setConstraints();
  
	// Creación de la tabla de costos entre las ciudades
	data->makeCostTable();
	
	// Cantidad de intentos con diferentes condiciones iniciales
	int nTries = (argc>1 ? atoi(argv[1]) : 10); // amount of tries
	
	// Creación de puntero a objeto de clase TSP
	TSP* tsp = new TSP[nTries]();
	
	// Seteo de semilla para rand()
	srand(time(NULL));
	
	// Cálculo mediante diferentes condiciones iniciales
	for(int i=0; i<nTries; i++){
		// Impresión
		cout<<"\n Intento: "<<i<<endl;
		
		// Carga de parámetros
		tsp[i].kt = (argc>2 ? atoi(argv[2]) : 10); // kt in boltzmann probability
		tsp[i].maxIter = (argc>3 ? atoi(argv[3]) : 10000); // maximum iterations
	  
		// Sorteo de la condición inicial
		tsp[i].initialCondition(data->nCities, data->prices, data->constraints);
	  
		// Algoritmo montecarlo para mejorar el camino
		tsp[i].mc(data->nCities, data->prices);
	}
	
	// Búsqueda de la mejor solución hallada
	double bestCost = tsp[0].bestCost;
	int* bestPath = tsp[0].bestPath;
	for (int i=1; i<nTries; i++){
		if(tsp[i].bestCost < bestCost){
			bestCost = tsp[i].bestCost;
			bestPath = tsp[i].bestPath;
		}
	}
	
	cout<<"\n\n Distancia total: "<<bestCost<<endl;
	cout<<"\n Mejor camino: "<<endl;
	
	for (int i=0; i<data->nCities; i++){
		cout<<" "<<bestPath[i]<<" "<<data->cities[bestPath[i]]<<endl;
	}
	cout<<" "<<bestPath[0]<<" "<<data->cities[bestPath[0]]<<endl;
  
	return 0;
}
