from numpy import mean,cov,double,cumsum,dot,linalg,array,rank,loadtxt, amax, amin, zeros ,cross, linalg, arccos

def pca(A):
	""" Analisis de componentes principales (PCA)
	Input: matriz A (n,p): n observaciones, p variables
	Output: matriz coeff (p,p), cada columna contiene coeficientes de una componente principal
		 matriz score (n,p), representacion de A en el espacio de componentes principales
		 vector latent (1, p), contiene los autovalores de la matriz de covarianzas de A """
	# Calculo de autovectores y autovalores de la matriz de covarianzas de A
	M = (A-mean(A.T,axis=1)).T # sustraccion del punto medio
	[latent,coeff] = linalg.eig(cov(M)) # autovalores y autovectores
	score = dot(coeff.T,M) # proyeccion de los puntos en el nuevo espacio
	return coeff,score,latent

def checkHighPC(score):
	""" Chequeo de la componente principal que define la altura del cilindro
	Input: matriz score (n,p), representacion de puntos originales en el espacio de componentes principales
	Output: indice de componente principal que define la altura """
	for i0 in range (3):
		i1,i2={0,1,2}-{i0}
		posibleRadius = max(amax(score[i1]), amax(score[i2]))
		maxRadius = amax((score[i1]**2+score[i2]**2)**0.5)
		if maxRadius <1.1*posibleRadius:
			return i0


A = loadtxt("cilindro.asc")# Carga de datos

coeff, score, latent = pca(A) # PCA
highPC = checkHighPC(score)

# Visualizacion de puntos mapeados
"""
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.plot(score[0], score[1], score[2])
plt.show()
"""

# altura aproximada mediante PCA
hmax,hmin = amax(score[highPC]), amin(score[highPC])
approximateH = hmax-hmin
print "\nAltura aproximada mediante PCA:", approximateH

# radio aproximado mediante PCA
approximateR = (score[(1+highPC)%3]**2 + score[(2+highPC)%3]**2)**0.5
maxApproximateR = amax(approximateR)
print "Radio aproximado mediante PCA:", maxApproximateR

# Inclinacion aproximada con el eje z
approximatePhi = arccos(coeff[2, highPC]/linalg.norm(coeff[:,highPC]))
print "Angulo aproximado con el eje z:", approximatePhi, "radianes"

# Normal a tapa superior del cilindro en sistema con ejes de componentes principales
# 3 puntos que pertenezcan a la tapa superior (cuyo radio es menor al radio aproximado)
p = []
for ri,xi,yi,zi in zip(approximateR,*score):
	if ri <0.9*maxApproximateR and xi>0.9*hmax:# radio inferior al maximo aproximado y lado tapa superior
		p += [array([xi,yi,zi])]
		if len(p) == 3: break
normal = cross(p[1]-p[0], p[2]-p[0])
normal = normal / linalg.norm(normal)

# Obtencion de nuevo sistema con ejes: npc1, npc2, npc3
npc1 = dot(coeff,normal)		# proyeccion de la normal en el espacio original
npc2 = [1,- npc1[0]/npc1[1],0]	# definimos arbitrariamente punto (1, y, 0) perteneciente al plano con normal npc1
npc2 = npc2 / linalg.norm(npc2)
npc3 = cross(npc1, npc2)
newCoeff = array([npc1,npc2,npc3])
# Mapeo de puntos en A a sistema con ejes npc1, npc2, npc3: nuevo score
M = (A-mean(A,axis=0)).T # sustraccion del punto medio
newScore = dot(newCoeff, M) # proyeccion de los puntos en el nuevo espacio

# Buscamos altura, radio e inclinacion
# Altura
hmax,hmin = amax(newScore[0]), amin(newScore[0])
h = hmax-hmin
# Radio
r = (newScore[1]**2 + newScore[2]**2)**0.5
maxRadius = amax(r)
radius = mean([ ri for ri,z in zip(r,newScore[0]) if(z<0.99*hmax and z>0.99*hmin)])
# Inclinacion con el eje z
phi = arccos(npc1[2]/linalg.norm(npc1))

print "\nAltura del cilindro:", h
print "Radio del cilindro:", radius
print "Angulo con el eje z:", phi, "radianes"
