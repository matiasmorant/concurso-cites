# La inclinación del cilindro

El problema fué resuelto por dos métodos: Análisis de Componentes Principales (PCA) y Regresión por Cuadrados Mínimos (LSQR)

Se incluyen dos códigos, ambos en lenguaje python y utilizando las bibliotecas numpy y scipy.

##Análisis de Componentes Principales (PCA)

La idea en la que se basa este método es que las componentes principales de la nube de puntos estarán aproximadamente alineadas, una con el vector dirección del cilindro y las otras dos serán normales al vector dirección (es decir, apuntando en dirección del vector 'radio'). Teniendo los componentes principales, podemos conocer la orientación del cilindro, y ya teniendo esa información es más fácil calcular el resto de los datos.

El algoritmo implementado en pca.py ejecuta los siguientes pasos:

1. Carga de datos:
	Lectura de "cilindro.asc".
2. Análisis de componentes principales:
	Implementado mediante cálculo de matriz de covarianza de los puntos leídos, y análisis de autovalores y autovectores de la misma.
3. Cálculo de la altura, radio e inclinación del cilindro aproximados:
	Se calculan a partir de los puntos mapeados a un sistema cuyos ejes son las componentes principales.
4. Cálculo de la dirección del eje del cilindro:
	Se calcula como la normal al plano definido por los puntos hallados en una de las tapas.
5. Diseño de un sistema de ejes ortogonales:
	El primer eje es la normal hallada previamente, los otros son arbitrarios pero ortogonales a él y entre si.
6. Cálculo de la altura, radio e inclinación del cilindro exactos:
	Se calculan a partir de los puntos mapeados a un sistema cuyos ejes son los hallados previamente.

##Regresión por Cuadrados Mínimos (LSQR)

Primero comenzamos por encontrar la ecuación que describe un cilindro.
La ecuación que describe un rectángulo de lados $2a$ y $2b$ en dos dimensiones es:
$$|\frac{y}{b}+\frac{x}{a} |+ |\frac{y}{b}-\frac{x}{a}|=2$$
Por otro lado, para un punto $X$ cualquiera, podemos calcular $cN$, la componente 'normal' al cilindro (la componente en la dirección normal al vector de dirección del cilindro), y $cT$, la componente 'tangencial' (la componente  en la misma dirección que el vector de dirección del cilindro), como:
$$cT=(X-C).T$$
$$cN=|(X - C) - ((X - C).T|$$

Si proyectamos el cilindro de tal manera que estas dos componentes queden sobre un plano, veremos un rectangulo de lados $2r$ y $h$, de aqui deducimos que la ecuación para el cilindro es

$$|\frac{cN}{r}+\frac{cT}{h/2} |+ |\frac{cN}{r}-\frac{cT}{h/2}|=2$$

Un punto X que no esté en la superficie del cilindro no cumplirá con esta ecuación, sino que tendrá un error $\epsilon$

$$\epsilon(X)=|\frac{cN}{r}+\frac{cT}{h/2} |+ |\frac{cN}{r}-\frac{cT}{h/2}|-2$$

Si calculamos este error para cada punto $X_i$, tendremos un vector de errores $\epsilon$ de componentes $\epsilon_i$. Si tomamos la norma de este vector, podemos estimar el error global resultante de elegir el cilindro dado para el ajuste.
Teniendo esta función de error podemos usar una función de optimización numérica ya implementada (scipy.optimize.minimize) para que encuentre parámetros que minimicen el error.

El resultado con ambos programas es 
```
Altura del cilindro: 17.08
Radio del cilindro: 7.389
Angulo con el eje z: 0.57440 radianes

```
Para ejecutar los programas escriba 
```
python2.7 pca.py
python2.7 lsqr.py
```
en la terminal.

