from numpy import loadtxt, ones, arccos, outer
from numpy.linalg import norm
from scipy.optimize import minimize

P = loadtxt("cilindro.asc") #cargamos los datos

def split(x):
	"""
	Input: ndarray x, vector de parametros
	Output: T,C,R,SH -> Tangent, Centro, Radius, SemiHeight
	"""
	return x[:3],x[3:6],x[6],x[7]

def squares_sum(x):
	"""
	funcion que devuelve el error al ajustar un cilindro con vector de parametros 'x' 
	Input: x, vector de parametros
	Output: error en el ajuste
	"""
	T,C,r,sh = split(x)
	cT = (P-C).dot(T)						#componente 'tangencial' al cilindro
	cN = norm((P-C) - outer(cT , T),axis=1)	#componente 'normal' al cilindro
	return norm(abs(cN/r + cT/sh) + abs(cN/r - cT/sh)-2) #norma del vector error

# minimizamos la funcion objetivo squares_sum, partiendo del valor inicial ones(8)
# y luego tomamos solo el resultado del objeto que nos devuelve minimize
x = minimize(squares_sum, ones(8)).x 
T,C,r,sh = split(x) # partimos el vector de parametros en los valores correspondientes
T = T/norm(T)		# normalizamos T para que la componente Tz sea el coseno director correspondiente a Z

print "\nAltura del cilindro:", sh*2
print "Radio del cilindro:", r
print "Angulo con el eje z:", arccos(T[2]), "radianes"


