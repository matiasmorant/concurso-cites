#El primo de Fibonacci
##Calculo de los primos

Existe un teorema que afirma que para cada $n \geq 3$, $f_n$ divide a $f_m$ si y sólo si $n$ divide a $m$ (donde $f_n$ es el número de Fibonacci con índice $n$). Esto implica que $f_n$ será primo sólo si $n$ es primo o $n = 4$. Usando este teorema, podemos generar todos los números primos $p_i$ tal que $p_i\le90$, luego calcular $f_{p_i}$ y averiguar si es primo usando alguno de los muchos algoritmos existentes. 

Ese es el algoritmo que utiliza el código 1a.py. Para ejecutarlo, escriba en el terminal

```
python2.7 1a.py
```

El resultado es 

$$2, 3, 5, 13, 89, 233, 1597, 28657, 514229, 433494437, 2971215073$$

#Calculo del numero de digitos

El número $f_n$ se puede expresar cómo

$$f_n=\frac{\phi ^n-(1-\phi )^n}{\sqrt{5}}$$

Donde $\phi=\frac{1}{2} \left(1+\sqrt{5}\right)$.

El número $f_{1477}$ es un número muy grande. Podemos evitar hacer el cómputo de este número para luego contar cuántos dígitos tiene notando primero que:

$$f_n\approx\frac{\phi ^n}{\sqrt{5}}$$

para $n$ grandes, ya que $1-\phi<1$ y cuánto más grande sea $n$, más pequeño se hará el término $(1-\phi )^n$.

También debemos notar que podemos usar la función $digitos(x) = \lfloor log_{10} (x) + 1 \rfloor$ para saber el número de dígitos de un número $x$.
Si aplicamos la función $digitos$ a la ecuación anterior nos queda

$$\lfloor log_{10} (\frac{\phi ^n}{\sqrt{5}}) + 1 \rfloor = \lfloor n \log _{10}(\phi)-\log _{10}(\sqrt{5})+1\rfloor$$

en esta última expresión podemos substituir $n$ por 1477 y calcular el resultado con una calculadora. El resultado es 309.


