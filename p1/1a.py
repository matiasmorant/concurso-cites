sqrt5 = 5**.5
phi  = (1+sqrt5)/2

def fib(n):
	"""
	devuelve el n-esimo numero de Fibonacci
	Input: n
	Output: n-esimo numero de Fibonacci
	"""
	return int((phi**n-(1-phi)**n)/sqrt5)

def is_prime(n):
	"""
	Input: entero n
	Output: True si n es primo y False si no lo es
	"""
	if n==2: return True	# 2 es primo
	if n%2==0: return False	# los numeros pares no son primos
	r = int(n**.5)			# los numeros mayores que r no pueden dividir n
	for i in range(3,r+1,2):# no dividimos por numeros pares, porque no son primos
		if n%i == 0:
			return False
	return True

indices = [p for p in range(3,91) if is_prime(p) or p==4]	# todos los numeros primos menores a 90
fibs = map(fib,indices)
print filter(is_prime,fibs)
